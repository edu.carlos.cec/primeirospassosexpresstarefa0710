const express = require('express');
const app = express();
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({entended:true}));

app.get("/", (req, res) => {
    res.sendFile(__dirname + "/views/index.html");
})

app.get("/views/index.html", (req, res) => {
    res.sendFile(__dirname + "/views/index.html");
})

app.get("/views/sobre.html", (req, res) => {
    res.sendFile(__dirname + "/views/sobre.html");
})


app.get("/views/contato.html", (req, res) => {
    res.sendFile(__dirname + "/views/contato.html");
})

app.post("/confirmacao", (req, res) => {
    res.send("Obrigado " + req.body.nome + " por ter enviado a mensagem " + req.body.msg + ". Retornaremos no e-mail " + req.body.email + ".");
});

//Ultima coisa do código
app.listen(3000, ()=>{
    console.log("O servidor está funcioando");
});

